﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Interfaces;

namespace Infrastructure.Services
{
    public class DateTimeOffsetService : IDateTimeOffset
    {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}
