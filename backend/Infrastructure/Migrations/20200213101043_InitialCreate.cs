﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExampleEntityLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExampleEntityLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TodoListEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TodoListEntities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExampleEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModified = table.Column<DateTimeOffset>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    ExampleEnum = table.Column<int>(nullable: false),
                    ExampleEntityListId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExampleEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExampleEntities_ExampleEntityLists_ExampleEntityListId",
                        column: x => x.ExampleEntityListId,
                        principalTable: "ExampleEntityLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TodoListItemEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Done = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TodoListItemEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TodoListItemEntities_TodoListEntities_ParentId",
                        column: x => x.ParentId,
                        principalTable: "TodoListEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExampleEntities_ExampleEntityListId",
                table: "ExampleEntities",
                column: "ExampleEntityListId");

            migrationBuilder.CreateIndex(
                name: "IX_TodoListItemEntities_ParentId",
                table: "TodoListItemEntities",
                column: "ParentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExampleEntities");

            migrationBuilder.DropTable(
                name: "TodoListItemEntities");

            migrationBuilder.DropTable(
                name: "ExampleEntityLists");

            migrationBuilder.DropTable(
                name: "TodoListEntities");
        }
    }
}
