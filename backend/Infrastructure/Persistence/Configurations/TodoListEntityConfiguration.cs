﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class TodoListEntityConfiguration : IEntityTypeConfiguration<TodoListEntity>
    {
        public void Configure(EntityTypeBuilder<TodoListEntity> builder)
        {
            builder.Property(e => e.Name)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}