﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.ExampleEntities.Queries.GetExampleEntities;
using AutoMapper;
using Infrastructure.Persistence;
using Shouldly;
using Xunit;

namespace Application.UnitTests.ExampleEntities.Queries
{
    [Collection("QueryTests")]
    public class GetExampleEntitiesQueryTest
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetExampleEntitiesQueryTest(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task Handle_ReturnsCorrectVmAndExampleEntitiesCount()
        {
            var query = new GetExampleEntitiesQuery();

            var handler = new GetExampleEntitiesQuery.GetExampleEntitiesQueryHandler(_context, _mapper);

            var result = await handler.Handle(query, CancellationToken.None);

            result.ShouldBeOfType<ExampleEntitiesViewModel>();
            result.ExampleEntities.Count.ShouldBe(5);
        }
    }
}
