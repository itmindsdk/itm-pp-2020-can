﻿using Domain.Enums;

namespace Domain.Entities
{
    public class TodoListItemEntity
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public virtual TodoListEntity? Parent { get; set; }
        public string Description { get; set; }
        public bool Done { get; set;  }
        public bool Deleted { get; set; }
    }
}