﻿
using System.Collections.Generic;

namespace Domain.Entities
{
    public class TodoListEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual IList<TodoListItemEntity> Items { get; set; }

    }
}