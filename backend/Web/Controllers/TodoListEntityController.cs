﻿using System.Threading.Tasks;
using Application.ExampleEntities.Queries.GetExampleEntities;
using Application.TodoListEntities.Commands.CreateTodoListEntity;
using Application.TodoListEntities.Commands.DeleteTodoListEntity;
using Application.TodoListEntities.Queries.GetTodoListEntity;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class TodoListEntityController : ApiControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateTodoListEntityCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateTodoListEntityCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteTodoListEntityCommand
            {
                Id = id
            });
            return NoContent();
        }

        [HttpGet]
        public async Task<ActionResult<TodoListViewModel>> Get()
        {
            return await Mediator.Send(new GetTodoListEntityQuery());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoListViewModel>> Get(int id)
        {
            return await Mediator.Send(new GetTodoSingleListEntityQuery
            {
                Id = id
            });
        }
    }
}