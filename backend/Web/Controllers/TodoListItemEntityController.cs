﻿using System.Threading.Tasks;
using Application.TodoListItemEntities.Commands.CreateTodoListItemEntity;
using Application.TodoListItemEntities.Commands.DeleteTodoListItemEntity;
using Application.TodoListItemEntities.Commands.UpdateTodoListItemEntity;
using Application.TodoListItemEntities.Queries.GetTodoListItemEntity;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("api/TodoList/item")]
    public class TodoListItemEntityController : ApiControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateTodoListItemEntityCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateTodoListItemEntityCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteTodoListItemEntityCommand
            {
                Id = id
            });
            return NoContent();
        }
        
        [HttpGet]
        public async Task<ActionResult<TodoListItemViewModel>> Get()
        {
            return await Mediator.Send(new GetAllTodoListItemsEntityQuery());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TodoListItemViewModel>> Get(int id)
        {
            return await Mediator.Send(new GetTodoListItemEntityQuery
            {
                Id = id
            });
        }
    }
}