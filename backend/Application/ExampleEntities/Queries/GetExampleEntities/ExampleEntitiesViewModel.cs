﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Enums;

namespace Application.ExampleEntities.Queries.GetExampleEntities
{
    public class ExampleEntitiesViewModel
    {
        public IList<ExampleEnumDto> ExampleEnum =
            Enum.GetValues(typeof(ExampleEnum))
                .Cast<ExampleEnum>()
                .Select(p => new ExampleEnumDto { Value = (int)p, Name = p.ToString() })
                .ToList();

        public IList<ExampleEntityDto> ExampleEntities { get; set; }
    }
}
