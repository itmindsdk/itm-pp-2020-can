﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.ExampleEntities.Queries.GetExampleEntities
{
    public class ExampleEntityListDto : IMapFrom<ExampleEntityList>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ExampleEntityList, ExampleEntityListDto>();
        }
    }
}
