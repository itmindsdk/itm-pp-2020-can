﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.ExampleEntities.Queries.GetExampleEntities
{
    public class ExampleEntityDto : IMapFrom<ExampleEntity>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ExampleEntityListDto ExampleEntityList { get; set; } 
        public int ExampleEnum { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ExampleEntity, ExampleEntityDto>()
                .ForMember(d => d.ExampleEnum, opt => opt.MapFrom(s => (int) s.ExampleEnum));


           
        }
    }
}
