﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.ExampleEntities.Queries.GetExampleEntities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListEntities.Queries.GetTodoListEntity
{
    public class GetTodoListEntityQuery : IRequest<TodoListViewModel>
    {
        public class GetTodoListEntityQueryHandler : IRequestHandler<GetTodoListEntityQuery, TodoListViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListViewModel> Handle(GetTodoListEntityQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new TodoListViewModel();

                viewModel.TodoListEntities = await _context.TodoListEntities
                    .ProjectTo<TodoListEntityDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return viewModel;
            }
        }
    }
}