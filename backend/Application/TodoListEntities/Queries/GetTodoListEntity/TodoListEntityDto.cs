﻿using System.Collections.Generic;
using Application.Common.Mappings;
using Application.ExampleEntities.Queries.GetExampleEntities;
using Application.TodoListItemEntities.Queries.GetTodoListItemEntity;
using AutoMapper;
using Domain.Entities;

namespace Application.TodoListEntities.Queries.GetTodoListEntity
{
    public class TodoListEntityDto : IMapFrom<TodoListEntity>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual IList<TodoListItemEntityDto> Items { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoListEntity, TodoListEntityDto>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(d => d.Name, opt => opt.MapFrom(s => (string) s.Name))
            .ForMember(d => d.Items, opt => opt.MapFrom(s => s.Items));
        }
    }
    
    
}