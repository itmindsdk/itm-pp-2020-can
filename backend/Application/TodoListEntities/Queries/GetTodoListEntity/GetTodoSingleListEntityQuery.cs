﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListEntities.Queries.GetTodoListEntity
{
    public class GetTodoSingleListEntityQuery : IRequest<TodoListViewModel>
    {
        public int Id { get; set; }
        
        public class GetTodoListEntityQueryHandler : IRequestHandler<GetTodoSingleListEntityQuery, TodoListViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListViewModel> Handle(GetTodoSingleListEntityQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new TodoListViewModel();

                viewModel.TodoListEntities = await _context.TodoListEntities
                    .ProjectTo<TodoListEntityDto>(_mapper.ConfigurationProvider)
                    .Where(dto => dto.Id == request.Id)
                    .DefaultIfEmpty()
                    .ToListAsync(cancellationToken);

                if (viewModel.TodoListEntities[0].Name == null)
                {
                    throw new NotFoundException(nameof(TodoListEntityDto), request.Id);
                }

                return viewModel;
            }
        }
    }
}