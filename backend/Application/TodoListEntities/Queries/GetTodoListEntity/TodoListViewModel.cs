﻿using System.Collections.Generic;

namespace Application.TodoListEntities.Queries.GetTodoListEntity
{
    public class TodoListViewModel
    {
        public IList<TodoListEntityDto> TodoListEntities { get; set; }
    }
}