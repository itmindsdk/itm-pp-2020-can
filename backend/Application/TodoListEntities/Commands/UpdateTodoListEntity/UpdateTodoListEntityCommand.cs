﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListEntities.Queries.GetTodoListEntity;
using AutoMapper;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListEntities.Commands.CreateTodoListEntity
{
    public class UpdateTodoListEntityCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public class UpdateTodoListEntityCommandHandler : IRequestHandler<UpdateTodoListEntityCommand>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public UpdateTodoListEntityCommandHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Unit> Handle(UpdateTodoListEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListEntity = await _context.TodoListEntities.FindAsync(request.Id);

                if (todoListEntity == null)
                {
                    throw new NotFoundException(nameof(TodoListEntity), request.Id);
                }

                todoListEntity = _mapper.Map<TodoListEntity>(request);

                //todoListEntity.Name = request.Name;

                _context.TodoListEntities.Update(todoListEntity);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}