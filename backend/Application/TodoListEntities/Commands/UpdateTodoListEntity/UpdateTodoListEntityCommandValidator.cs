﻿using FluentValidation;

namespace Application.TodoListEntities.Commands.CreateTodoListEntity
{
    public class UpdateTodoListEntityCommandValidator : AbstractValidator<CreateTodoListEntityCommand>
    {
        public UpdateTodoListEntityCommandValidator()
        {
            RuleFor(e => e.Name)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}