﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Common.Mappings;
using Application.TodoListItemEntities.Queries.GetTodoListItemEntity;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TodoListEntities.Commands.CreateTodoListEntity
{
    public class CreateTodoListEntityCommand : IRequest<int>
    {
        public string Name { get; set; }

        public class CreateTodoListEntityCommandHandler : IRequestHandler<CreateTodoListEntityCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateTodoListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateTodoListEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListEntity = new TodoListEntity
                {
                    Name = request.Name
                };

                _context.TodoListEntities.Add(todoListEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return todoListEntity.Id;
            }
        }
    }
}