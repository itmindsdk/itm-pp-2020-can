﻿using FluentValidation;

namespace Application.TodoListEntities.Commands.CreateTodoListEntity
{
    public class CreateTodoListEntityCommandValidator : AbstractValidator<CreateTodoListEntityCommand>
    {
        public CreateTodoListEntityCommandValidator()
        {
            RuleFor(e => e.Name)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}