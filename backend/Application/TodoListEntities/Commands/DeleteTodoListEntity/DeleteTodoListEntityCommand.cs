﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListItemEntities.Queries.GetTodoListItemEntity;
using Domain.Entities;
using MediatR;

namespace Application.TodoListEntities.Commands.DeleteTodoListEntity
{
    public class DeleteTodoListEntityCommand : IRequest
    {
        public int Id { get; set; }
        
        public class DeleteTodoListEntityCommandHandler : IRequestHandler<DeleteTodoListEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteTodoListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<Unit> Handle(DeleteTodoListEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListEntity = await _context.TodoListEntities.FindAsync(request.Id);

                if (todoListEntity == null)
                {
                    throw new NotFoundException(nameof(TodoListEntity), request.Id);
                }

                /*foreach (TodoListItemEntity item in todoListEntity.Items)
                {
                    _context.TodoListItemEntities.Remove(item);
                }*/
                
                _context.TodoListEntities.Remove(todoListEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}