﻿using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {


        DbSet<ExampleEntity> ExampleEntities { get; set; }

        DbSet<ExampleEntityList> ExampleEntityLists { get; set; }
        
        DbSet<TodoListEntity> TodoListEntities { get; set; }
        
        DbSet<TodoListItemEntity> TodoListItemEntities { get; set; }
        
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
