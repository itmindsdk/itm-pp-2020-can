﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoListItemEntities.Commands.DeleteTodoListItemEntity
{
    public class DeleteTodoListItemEntityCommand : IRequest
    {
        public int Id { get; set; }
        
        public class DeleteTodoListItemEntityCommandHandler : IRequestHandler<DeleteTodoListItemEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteTodoListItemEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<Unit> Handle(DeleteTodoListItemEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListItemEntity = await _context.TodoListItemEntities.FindAsync(request.Id);

                if (todoListItemEntity == null)
                {
                    throw  new NotFoundException(nameof(TodoListItemEntity), request.Id);
                }

                todoListItemEntity.Deleted = true;

                _context.TodoListItemEntities.Update(todoListItemEntity);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}