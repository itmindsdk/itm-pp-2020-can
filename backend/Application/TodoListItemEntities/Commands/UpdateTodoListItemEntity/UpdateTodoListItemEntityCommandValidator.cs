﻿using Application.TodoListItemEntities.Commands.CreateTodoListItemEntity;
using FluentValidation;

namespace Application.TodoListItemEntities.Commands.UpdateTodoListItemEntity
{
    public class UpdateTodoListItemEntityCommandValidator : AbstractValidator<CreateTodoListItemEntityCommand>
    {
        public UpdateTodoListItemEntityCommandValidator()
        {
            RuleFor(e => e.Description)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}