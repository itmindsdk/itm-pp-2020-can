﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;

namespace Application.TodoListItemEntities.Commands.UpdateTodoListItemEntity
{
    public class UpdateTodoListItemEntityCommand : IRequest
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Done { get; set; }

        public class UpdateTodoListItemEntityCommandHandler : IRequestHandler<UpdateTodoListItemEntityCommand>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public UpdateTodoListItemEntityCommandHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Unit> Handle(UpdateTodoListItemEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListItemEntity = await _context.TodoListItemEntities.FindAsync(request.Id);

                if (todoListItemEntity == null)
                {
                    throw  new NotFoundException(nameof(TodoListItemEntity), request.Id);
                }

                todoListItemEntity.Description = request.Description;
                todoListItemEntity.Done = request.Done;

                _context.TodoListItemEntities.Update(todoListItemEntity);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}