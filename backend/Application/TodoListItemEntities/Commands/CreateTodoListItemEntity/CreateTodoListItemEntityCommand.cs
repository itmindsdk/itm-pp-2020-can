﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListEntities.Commands.CreateTodoListEntity;
using Application.TodoListEntities.Queries.GetTodoListEntity;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListItemEntities.Commands.CreateTodoListItemEntity
{
    public class CreateTodoListItemEntityCommand : IRequest<int>
    {
        public int ParentId { get; set; }
        public virtual TodoListEntity Parent { get; set; }
        public string Description { get; set; }
        public bool Done { get; set; }

        public class CreateTodoListItemEntityCommandHandler : IRequestHandler<CreateTodoListItemEntityCommand, int>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public CreateTodoListItemEntityCommandHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<int> Handle(CreateTodoListItemEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListItemEntity = //_mapper.Map<TodoListItemEntity>(request);
                    new TodoListItemEntity
                {
                    ParentId = request.ParentId,
                    //Parent = request.Parent,
                    Description = request.Description,
                    Done = request.Done
                };
                
                var parent = new TodoListEntityDto();

                parent = await _context.TodoListEntities
                    .ProjectTo<TodoListEntityDto>(_mapper.ConfigurationProvider)
                    .Where(dto => dto.Id == request.ParentId)
                    .FirstOrDefaultAsync(cancellationToken);
                
                if (parent == null)
                {
                    throw new NotFoundException(nameof(TodoListEntity), request.ParentId);
                }

                _context.TodoListItemEntities.Add(todoListItemEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return todoListItemEntity.Id;
            }
        }
    }
}