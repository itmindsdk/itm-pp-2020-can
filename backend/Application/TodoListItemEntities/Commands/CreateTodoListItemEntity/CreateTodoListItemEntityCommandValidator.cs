﻿using FluentValidation;

namespace Application.TodoListItemEntities.Commands.CreateTodoListItemEntity
{
    public class CreateTodoListItemEntityCommandValidator : AbstractValidator<CreateTodoListItemEntityCommand>
    {
        public CreateTodoListItemEntityCommandValidator()
        {
            RuleFor(e => e.Description)
                .MaximumLength(200);
        }
    }
}