﻿using System.Collections.Generic;

namespace Application.TodoListItemEntities.Queries.GetTodoListItemEntity
{
    public class TodoListItemViewModel
    {
        public IList<TodoListItemEntityDto> TodoListItemEntities { get; set; }
    }
}