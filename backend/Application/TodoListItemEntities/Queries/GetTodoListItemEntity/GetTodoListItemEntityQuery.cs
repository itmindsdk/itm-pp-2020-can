﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListItemEntities.Queries.GetTodoListItemEntity
{
    public class GetTodoListItemEntityQuery : IRequest<TodoListItemViewModel>
    {
        
        public int Id { get; set; }

        public class GetTodoListItemEntityQueryHandler : IRequestHandler<GetTodoListItemEntityQuery, TodoListItemViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListItemEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListItemViewModel> Handle(GetTodoListItemEntityQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new TodoListItemViewModel();

                viewModel.TodoListItemEntities = await _context.TodoListItemEntities
                    .ProjectTo<TodoListItemEntityDto>(_mapper.ConfigurationProvider)
                    .Where(dto => dto.Id == request.Id && !dto.Deleted)
                    .ToListAsync(cancellationToken);

                if (viewModel.TodoListItemEntities.Count == 0)
                {
                    throw new NotFoundException(nameof(TodoListItemEntity), request.Id);
                }
                
                return viewModel;
            }
        }
    }
}