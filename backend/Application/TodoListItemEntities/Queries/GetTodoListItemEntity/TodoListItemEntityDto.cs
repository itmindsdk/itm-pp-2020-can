﻿using Application.Common.Mappings;
using Application.TodoListEntities.Queries.GetTodoListEntity;
using AutoMapper;
using Domain.Entities;

namespace Application.TodoListItemEntities.Queries.GetTodoListItemEntity
{
    public class TodoListItemEntityDto : IMapFrom<TodoListItemEntity>
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Description { get; set; }
        public bool Done { get; set; }
        public bool Deleted { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoListItemEntity, TodoListItemEntityDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.ParentId, opt => opt.MapFrom(s => s.ParentId))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
                .ForMember(d => d.Done, opt => opt.MapFrom(s => s.Done))
                .ForMember(d => d.Deleted, opt => opt.MapFrom(s => s.Deleted));
        }
    }
}