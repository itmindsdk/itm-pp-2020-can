﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListItemEntities.Queries.GetTodoListItemEntity
{
    public class GetAllTodoListItemsEntityQuery : IRequest<TodoListItemViewModel>
    {

        public class GetAllTodoListItemsEntityQueryHandler : IRequestHandler<GetAllTodoListItemsEntityQuery, TodoListItemViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAllTodoListItemsEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListItemViewModel> Handle(GetAllTodoListItemsEntityQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new TodoListItemViewModel();

                viewModel.TodoListItemEntities = await _context.TodoListItemEntities
                    .ProjectTo<TodoListItemEntityDto>(_mapper.ConfigurationProvider)
                    .Where((dto => !dto.Deleted))
                    .ToListAsync(cancellationToken);

                return viewModel;
            }
        }
    }
}