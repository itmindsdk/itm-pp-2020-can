﻿using System.Threading.Tasks;
using Application.ExampleEntities.Commands.UpdateExampleEntity;
using Domain.Enums;
using Xunit;

namespace Web.IntegrationTests.Controllers.ExampleEntities
{
    public class Update : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Update(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenValidUpdateExampleEntityCommand_ReturnsSuccessCode()
        {
            var client = _factory.GetAnonymousClient();

            var command = new UpdateExampleEntityCommand
            {
                Id = 2,
                Name = "Do this thing.",
                ExampleEnum = ExampleEnum.D
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PutAsync($"/api/ExampleEntity/{command.Id}", content);

            response.EnsureSuccessStatusCode();
        }
    }
}
