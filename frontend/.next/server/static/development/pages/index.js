module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./NSwagTS/backend-api.ts":
/*!********************************!*\
  !*** ./NSwagTS/backend-api.ts ***!
  \********************************/
/*! exports provided: AuthClient, ClientBase, ExampleEntityClient, ExampleEntityListClient, TodoListEntityClient, CreateExampleEntityCommand, ExampleEnum, UpdateExampleEntityCommand, ExampleEntitiesViewModel, ExampleEnumDto, ExampleEntityDto, ExampleEntityListDto, CreateExampleEntityListCommand, CreateTodoListEntityCommand, UpdateTodoListEntityCommand, TodoListViewModel, TodoListEntityDto, SwaggerException */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthClient", function() { return AuthClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientBase", function() { return ClientBase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityClient", function() { return ExampleEntityClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityListClient", function() { return ExampleEntityListClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListEntityClient", function() { return TodoListEntityClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateExampleEntityCommand", function() { return CreateExampleEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEnum", function() { return ExampleEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateExampleEntityCommand", function() { return UpdateExampleEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntitiesViewModel", function() { return ExampleEntitiesViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEnumDto", function() { return ExampleEnumDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityDto", function() { return ExampleEntityDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityListDto", function() { return ExampleEntityListDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateExampleEntityListCommand", function() { return CreateExampleEntityListCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTodoListEntityCommand", function() { return CreateTodoListEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateTodoListEntityCommand", function() { return UpdateTodoListEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListViewModel", function() { return TodoListViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListEntityDto", function() { return TodoListEntityDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwaggerException", function() { return SwaggerException; });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* tslint:disable */

/* eslint-disable */
//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v13.1.6.0 (NJsonSchema v10.0.28.0 (Newtonsoft.Json v12.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------
// ReSharper disable InconsistentNaming
class AuthClient {
  constructor(accessToken) {
    this.accessToken = accessToken;
  }

  transformHttpRequestOptions(options) {
    if (options.headers && this.accessToken) {
      options.headers['Authorization'] = 'Bearer ' + this.accessToken;
      return Promise.resolve(options);
    }
  }

}
class ClientBase {
  constructor(authClient) {
    this.authClient = authClient;
  }

  transformOptions(options) {
    return this.authClient ? this.authClient.transformHttpRequestOptions(options) : Promise.resolve(options);
  }

}
class ExampleEntityClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command) {
    let url_ = this.baseUrl + "/api/ExampleEntity";
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  get() {
    let url_ = this.baseUrl + "/api/ExampleEntity";
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGet(_response);
    });
  }

  processGet(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = ExampleEntitiesViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  update(id, command) {
    let url_ = this.baseUrl + "/api/ExampleEntity/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processUpdate(_response);
    });
  }

  processUpdate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  delete(id) {
    let url_ = this.baseUrl + "/api/ExampleEntity/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "DELETE",
      headers: {
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processDelete(_response);
    });
  }

  processDelete(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class ExampleEntityListClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command) {
    let url_ = this.baseUrl + "/api/ExampleEntityList";
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class TodoListEntityClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command) {
    let url_ = this.baseUrl + "/api/TodoListEntity";
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  get() {
    let url_ = this.baseUrl + "/api/TodoListEntity";
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGet(_response);
    });
  }

  processGet(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = TodoListViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  update(id, command) {
    let url_ = this.baseUrl + "/api/TodoListEntity/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processUpdate(_response);
    });
  }

  processUpdate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  delete(id) {
    let url_ = this.baseUrl + "/api/TodoListEntity/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "DELETE",
      headers: {
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processDelete(_response);
    });
  }

  processDelete(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class CreateExampleEntityCommand {
  constructor(data) {
    _defineProperty(this, "name", void 0);

    _defineProperty(this, "exampleEnum", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.name = _data["name"];
      this.exampleEnum = _data["exampleEnum"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateExampleEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    data["exampleEnum"] = this.exampleEnum;
    return data;
  }

}
let ExampleEnum;

(function (ExampleEnum) {
  ExampleEnum[ExampleEnum["A"] = 0] = "A";
  ExampleEnum[ExampleEnum["B"] = 1] = "B";
  ExampleEnum[ExampleEnum["C"] = 2] = "C";
  ExampleEnum[ExampleEnum["D"] = 3] = "D";
})(ExampleEnum || (ExampleEnum = {}));

class UpdateExampleEntityCommand {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "exampleEnum", void 0);

    _defineProperty(this, "exampleEntityListId", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
      this.exampleEnum = _data["exampleEnum"];
      this.exampleEntityListId = _data["exampleEntityListId"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new UpdateExampleEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    data["exampleEnum"] = this.exampleEnum;
    data["exampleEntityListId"] = this.exampleEntityListId;
    return data;
  }

}
class ExampleEntitiesViewModel {
  constructor(data) {
    _defineProperty(this, "exampleEnum", void 0);

    _defineProperty(this, "exampleEntities", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      if (Array.isArray(_data["exampleEnum"])) {
        this.exampleEnum = [];

        for (let item of _data["exampleEnum"]) this.exampleEnum.push(ExampleEnumDto.fromJS(item));
      }

      if (Array.isArray(_data["exampleEntities"])) {
        this.exampleEntities = [];

        for (let item of _data["exampleEntities"]) this.exampleEntities.push(ExampleEntityDto.fromJS(item));
      }
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEntitiesViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};

    if (Array.isArray(this.exampleEnum)) {
      data["exampleEnum"] = [];

      for (let item of this.exampleEnum) data["exampleEnum"].push(item.toJSON());
    }

    if (Array.isArray(this.exampleEntities)) {
      data["exampleEntities"] = [];

      for (let item of this.exampleEntities) data["exampleEntities"].push(item.toJSON());
    }

    return data;
  }

}
class ExampleEnumDto {
  constructor(data) {
    _defineProperty(this, "value", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.value = _data["value"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEnumDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["value"] = this.value;
    data["name"] = this.name;
    return data;
  }

}
class ExampleEntityDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "exampleEntityList", void 0);

    _defineProperty(this, "exampleEnum", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
      this.exampleEntityList = _data["exampleEntityList"] ? ExampleEntityListDto.fromJS(_data["exampleEntityList"]) : undefined;
      this.exampleEnum = _data["exampleEnum"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEntityDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    data["exampleEntityList"] = this.exampleEntityList ? this.exampleEntityList.toJSON() : undefined;
    data["exampleEnum"] = this.exampleEnum;
    return data;
  }

}
class ExampleEntityListDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEntityListDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }

}
class CreateExampleEntityListCommand {
  constructor(data) {
    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateExampleEntityListCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    return data;
  }

}
class CreateTodoListEntityCommand {
  constructor(data) {
    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateTodoListEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    return data;
  }

}
class UpdateTodoListEntityCommand {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new UpdateTodoListEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }

}
class TodoListViewModel {
  constructor(data) {
    _defineProperty(this, "todoListEntities", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      if (Array.isArray(_data["todoListEntities"])) {
        this.todoListEntities = [];

        for (let item of _data["todoListEntities"]) this.todoListEntities.push(TodoListEntityDto.fromJS(item));
      }
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};

    if (Array.isArray(this.todoListEntities)) {
      data["todoListEntities"] = [];

      for (let item of this.todoListEntities) data["todoListEntities"].push(item.toJSON());
    }

    return data;
  }

}
class TodoListEntityDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListEntityDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }

}
class SwaggerException extends Error {
  constructor(message, status, response, headers, result) {
    super();

    _defineProperty(this, "message", void 0);

    _defineProperty(this, "status", void 0);

    _defineProperty(this, "response", void 0);

    _defineProperty(this, "headers", void 0);

    _defineProperty(this, "result", void 0);

    _defineProperty(this, "isSwaggerException", true);

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  static isSwaggerException(obj) {
    return obj.isSwaggerException === true;
  }

}

function throwException(message, status, response, headers, result) {
  if (result !== null && result !== undefined) throw result;else throw new SwaggerException(message, status, response, headers, null);
}

/***/ }),

/***/ "./src/components/NavBarComponent.tsx":
/*!********************************************!*\
  !*** ./src/components/NavBarComponent.tsx ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Navbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Navbar */ "react-bootstrap/Navbar");
/* harmony import */ var react_bootstrap_Navbar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Navbar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Button */ "react-bootstrap/Button");
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Container */ "react-bootstrap/Container");
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Form */ "react-bootstrap/Form");
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../NSwagTS/backend-api */ "./NSwagTS/backend-api.ts");
/* harmony import */ var _contexts_ClientContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../contexts/ClientContext */ "./src/contexts/ClientContext.tsx");
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../contexts/AppContext */ "./src/contexts/AppContext.tsx");
var _jsxFileName = "C:\\Users\\can\\RiderProjects\\itm-pp-2020-can\\frontend\\src\\components\\NavBarComponent.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;









const NavBarComponent = () => {
  const {
    0: name,
    1: setName
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const app = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_7__["appContext"]);
  const client = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_ClientContext__WEBPACK_IMPORTED_MODULE_6__["context"]);

  const handleClick = () => {
    if (name == "") return;
    client.todoListClient.create(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_5__["CreateTodoListEntityCommand"]({
      name: name
    })).then(value => {
      app.addTodoList(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_5__["TodoListEntityDto"]({
        id: value,
        name: name
      }));
      setName("");
    });
  };

  return __jsx("div", {
    style: {
      width: "100%",
      backgroundColor: "#f8f9fa",
      borderBottom: "3px solid rgb(238, 241, 243)",
      marginBottom: "1em"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: undefined
  }, __jsx(react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: undefined
  }, __jsx(react_bootstrap_Navbar__WEBPACK_IMPORTED_MODULE_1___default.a, {
    bg: "light",
    expand: "lg",
    style: {
      paddingRight: "0",
      paddingLeft: "0"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: undefined
  }, __jsx(react_bootstrap_Navbar__WEBPACK_IMPORTED_MODULE_1___default.a.Brand, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: undefined
  }, "Add todo list"), __jsx(react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_4___default.a.Control, {
    type: "text",
    placeholder: "List name",
    value: name,
    onChange: e => setName(e.target.value),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: undefined
  }), __jsx(react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_2___default.a, {
    variant: "primary",
    onClick: handleClick,
    style: {
      marginLeft: "1em"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: undefined
  }, "Add"))));
};

/* harmony default export */ __webpack_exports__["default"] = (NavBarComponent);

/***/ }),

/***/ "./src/components/TodoListComponent.tsx":
/*!**********************************************!*\
  !*** ./src/components/TodoListComponent.tsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\Users\\can\\RiderProjects\\itm-pp-2020-can\\frontend\\src\\components\\TodoListComponent.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const TodoListComponent = ({
  todoList
}) => {
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"].Item, {
    active: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }, todoList.name)));
};

/* harmony default export */ __webpack_exports__["default"] = (TodoListComponent);

/***/ }),

/***/ "./src/contexts/AppContext.tsx":
/*!*************************************!*\
  !*** ./src/contexts/AppContext.tsx ***!
  \*************************************/
/*! exports provided: appContext, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appContext", function() { return appContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppContextProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_ListReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util/ListReducer */ "./src/util/ListReducer.ts");
var _jsxFileName = "C:\\Users\\can\\RiderProjects\\itm-pp-2020-can\\frontend\\src\\contexts\\AppContext.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const appContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(null);
function AppContextProvider({
  children
}) {
  const {
    0: todoLists,
    1: todoListsDispatch
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useReducer"])(Object(_util_ListReducer__WEBPACK_IMPORTED_MODULE_1__["default"])("id"), []);
  const addTodoList = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(list => {
    todoListsDispatch({
      type: _util_ListReducer__WEBPACK_IMPORTED_MODULE_1__["ListReducerActionType"].Add,
      data: list
    });
  }, [todoListsDispatch]);
  const setTodoLists = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(lists => {
    todoListsDispatch({
      type: _util_ListReducer__WEBPACK_IMPORTED_MODULE_1__["ListReducerActionType"].Reset,
      data: lists
    });
  }, [todoListsDispatch]);
  return __jsx(appContext.Provider, {
    value: {
      todoLists,
      todoListsDispatch,
      addTodoList,
      setTodoLists
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }, children);
}
;

/***/ }),

/***/ "./src/contexts/ClientContext.tsx":
/*!****************************************!*\
  !*** ./src/contexts/ClientContext.tsx ***!
  \****************************************/
/*! exports provided: context, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "context", function() { return context; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ClientContextProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../NSwagTS/backend-api */ "./NSwagTS/backend-api.ts");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "C:\\Users\\can\\RiderProjects\\itm-pp-2020-can\\frontend\\src\\contexts\\ClientContext.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const context = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])({
  todoListClient: null
});
function ClientContextProvider({
  children
}) {
  return __jsx(context.Provider, {
    value: {
      todoListClient: new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["TodoListEntityClient"](new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["AuthClient"]("asd"), "https://localhost:5001", {
        fetch: (isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default())
      })
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, children);
}
;

/***/ }),

/***/ "./src/pages/TodoLists.tsx":
/*!*********************************!*\
  !*** ./src/pages/TodoLists.tsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contexts/AppContext */ "./src/contexts/AppContext.tsx");
/* harmony import */ var _components_TodoListComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/TodoListComponent */ "./src/components/TodoListComponent.tsx");
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Container */ "react-bootstrap/Container");
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "C:\\Users\\can\\RiderProjects\\itm-pp-2020-can\\frontend\\src\\pages\\TodoLists.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const TodoLists = () => {
  const context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__["appContext"]);
  return __jsx(react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_3___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: undefined
  }, context.todoLists.map(item => {
    return __jsx(_components_TodoListComponent__WEBPACK_IMPORTED_MODULE_2__["default"], {
      key: item.id,
      todoList: item,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: undefined
    });
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (TodoLists);

/***/ }),

/***/ "./src/pages/index.tsx":
/*!*****************************!*\
  !*** ./src/pages/index.tsx ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _contexts_ClientContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contexts/ClientContext */ "./src/contexts/ClientContext.tsx");
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contexts/AppContext */ "./src/contexts/AppContext.tsx");
/* harmony import */ var _components_NavBarComponent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/NavBarComponent */ "./src/components/NavBarComponent.tsx");
/* harmony import */ var _TodoLists__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./TodoLists */ "./src/pages/TodoLists.tsx");
var _jsxFileName = "C:\\Users\\can\\RiderProjects\\itm-pp-2020-can\\frontend\\src\\pages\\index.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const Index = () => {
  //const [todoList, setTodoList] = useState<TodoListEntityDto[]>([]);
  const client = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_ClientContext__WEBPACK_IMPORTED_MODULE_1__["context"]);
  const app = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__["appContext"]);
  /*const list = todoList.map((ting) => {
      return <h1>{ting.name}</h1>
  });*/

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    client.todoListClient.get().then(data => {
      app.setTodoLists(data.todoListEntities);
    });
  }, [client]);
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: undefined
  }, __jsx(_components_NavBarComponent__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: undefined
  }), __jsx(_TodoLists__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ "./src/util/ListReducer.ts":
/*!*********************************!*\
  !*** ./src/util/ListReducer.ts ***!
  \*********************************/
/*! exports provided: ListReducerActionType, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListReducerActionType", function() { return ListReducerActionType; });
/**
 * Different mutating types to be performed in the reducers dispatch.
 */
let ListReducerActionType; //The conditional type, specifying the type of data depending on the action type

(function (ListReducerActionType) {
  ListReducerActionType[ListReducerActionType["Remove"] = 0] = "Remove";
  ListReducerActionType[ListReducerActionType["Update"] = 1] = "Update";
  ListReducerActionType[ListReducerActionType["Add"] = 2] = "Add";
  ListReducerActionType[ListReducerActionType["AddOrUpdate"] = 3] = "AddOrUpdate";
  ListReducerActionType[ListReducerActionType["Reset"] = 4] = "Reset";
})(ListReducerActionType || (ListReducerActionType = {}));

/**
 *
 * @typeparam `T` type of the reducer state
 * @param {keyof T} key value of `U`
 * @return {Reducer} React reducer for a stateful list of `T`
 *
 * Can be initiated like this
 * `listReducer<Entity>("id")`
 * Where `Entity` is the type of the list
 * and `"id"` is a property key on the type
 * that is to be used to find index in the list
 */
/* harmony default export */ __webpack_exports__["default"] = (key => (state, action) => {
  const replace = t => {
    const index = state.findIndex(i => i[key] === t[key]);
    state[index] = t;
  };

  switch (action.type) {
    case ListReducerActionType.AddOrUpdate:
      if (action.data.push) {
        console.log("what?");
      } else {
        const index = state.findIndex(i => i[key] === action.data[key]);

        if (index !== -1) {
          replace(action.data);
          return [...state];
        } else {
          return [...state, action.data];
        }
      }

    case ListReducerActionType.Add:
      if (action.data.push) {
        return [...state, ...action.data];
      } else {
        return [...state, action.data];
      }

    case ListReducerActionType.Update:
      {
        if (action.data.push) {
          action.data.forEach(replace);
        } else {
          replace(action.data);
        }

        return [...state];
      }

    case ListReducerActionType.Remove:
      if (action.data.push) {
        return state.filter(t => action.data.indexOf(t[key]) === -1);
      } else {
        return state.filter(t => t[key] !== action.data);
      }

    case ListReducerActionType.Reset:
      return action.data;

    default:
      return state;
  }
});

/***/ }),

/***/ 4:
/*!***********************************!*\
  !*** multi ./src/pages/index.tsx ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\can\RiderProjects\itm-pp-2020-can\frontend\src\pages\index.tsx */"./src/pages/index.tsx");


/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-bootstrap/Button":
/*!*****************************************!*\
  !*** external "react-bootstrap/Button" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Button");

/***/ }),

/***/ "react-bootstrap/Container":
/*!********************************************!*\
  !*** external "react-bootstrap/Container" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Container");

/***/ }),

/***/ "react-bootstrap/Form":
/*!***************************************!*\
  !*** external "react-bootstrap/Form" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Form");

/***/ }),

/***/ "react-bootstrap/Navbar":
/*!*****************************************!*\
  !*** external "react-bootstrap/Navbar" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Navbar");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map