﻿import {createContext} from "react";
import {AuthClient, TodoListEntityClient, TodoListItemEntityClient} from "../../NSwagTS/backend-api";
import fetch from "isomorphic-unfetch";

type ClientContextType = {
    todoListClient : TodoListEntityClient
    todoListItemClient : TodoListItemEntityClient
}

export const context = createContext <ClientContextType>(null);

export default function ClientContextProvider({children}){
    return (
        <context.Provider value={{todoListClient: new TodoListEntityClient(new AuthClient("asd"), "https://localhost:5001", { fetch } ), todoListItemClient: new TodoListItemEntityClient(new AuthClient("aswdwd"), "https://localhost:5001", { fetch } )}}>
            {children}
        </context.Provider>
    );
};