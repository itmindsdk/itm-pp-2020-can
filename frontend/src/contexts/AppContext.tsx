﻿import {TodoListEntityDto, TodoListItemEntityDto} from "../../NSwagTS/backend-api";
import React, {createContext, Dispatch, useCallback, useReducer} from "react";
import ListReducer, {AllListActions, ListReducerActionType} from "../util/ListReducer";

type AppContextType = {
    todoLists: TodoListEntityDto[]
    todoListsDispatch: Dispatch<AllListActions<TodoListEntityDto>>
    todoListItems: TodoListItemEntityDto[]
    todoListItemsDispatch: Dispatch<AllListActions<TodoListItemEntityDto>>
    addTodoList: Function
    setTodoLists: Function
    addTodoListItem: Function
    removeTodoListItem: Function
    setTodoListItems: Function
    updateTodoListItem: Function
}
export const appContext = createContext<AppContextType>(null);

export default function AppContextProvider({children}){
    const [todoLists, todoListsDispatch] = useReducer(ListReducer<TodoListEntityDto>("id"), []);
    const [todoListItems, todoListItemsDispatch] = useReducer(ListReducer<TodoListItemEntityDto>("id"), []);

    const addTodoList = useCallback(
        (list: TodoListEntityDto) => {
            todoListsDispatch({
                type: ListReducerActionType.Add,
                data: list
            });
        },
        [todoListsDispatch]
    );

    const setTodoLists = useCallback(
        (lists: TodoListEntityDto[]) => {
            todoListsDispatch({
                type: ListReducerActionType.Reset,
                data: lists
            });
        },
        [todoListsDispatch]
    );
    
    const addTodoListItem = useCallback(
        (list: TodoListItemEntityDto) => {
            todoListItemsDispatch({
                type: ListReducerActionType.Add,
                data: list
            });
        },
        [todoListItemsDispatch]
    );
    
    const removeTodoListItem = useCallback(
        (number: number) => {
            todoListItemsDispatch({
                type: ListReducerActionType.Remove,
                data: number
            });
        },
        [todoListItemsDispatch]
    );
    
    const setTodoListItems = useCallback(
        (lists: TodoListItemEntityDto[]) => {
            todoListItemsDispatch({
                type: ListReducerActionType.Reset,
                data: lists
            });
        },
        [todoListItemsDispatch]
    );

    const updateTodoListItem = useCallback(
        (item: TodoListItemEntityDto) => {
            todoListItemsDispatch({
                type: ListReducerActionType.Update,
                data: item
            });
        },
        [todoListItemsDispatch]
    );
    
    return (
        <appContext.Provider value={{todoLists, todoListsDispatch, todoListItems, todoListItemsDispatch, addTodoList, setTodoLists, addTodoListItem, removeTodoListItem, setTodoListItems, updateTodoListItem}}>
            {children}
        </appContext.Provider>
    );
};