﻿import { createContext } from "react";
import {TodoListEntityClient, TodoListItemEntityClient} from '../../NSwagTS/backend-api';

export type ApiClientContextType = {
    clients: {
        todoListClient: TodoListEntityClient;
        todoListItemClient: TodoListItemEntityClient;
    };
};

export const ApiClientContext = createContext<ApiClientContextType>({
    clients: null,
});