﻿import 'bootstrap/dist/css/bootstrap.min.css';
import { AppProps } from 'next/app'
import ClientContextProvider from '../contexts/ClientContext'
import AppContextProvider from "../contexts/AppContext";
import React from "react";


function MyApp({ Component, pageProps }: AppProps) {
    return (
        <ClientContextProvider>
            <AppContextProvider>
                <Component {...pageProps} />
            </AppContextProvider>
        </ClientContextProvider>
    )
}

export default MyApp
