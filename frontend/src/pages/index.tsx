import React, {useContext, useEffect} from "react";
import {context} from "../contexts/ClientContext";
import { appContext } from "../contexts/AppContext"
import NavBarComponent from "../components/NavBarComponent"
import {NextPage} from "next";
import Container from "react-bootstrap/Container";
import TodoListComponent from "../components/TodoListComponent";

const Index : NextPage = () => {
  
  const client = useContext(context);
  const app = useContext(appContext);

    useEffect(() => {
        client.todoListClient.get()
            .then(listData => {
                app.setTodoLists(listData.todoListEntities);
            });
        client.todoListItemClient.get()
            .then(itemData => {
                app.setTodoListItems(itemData.todoListItemEntities)
            })
    }, [client]);
    
  return (
      <div>
          <NavBarComponent/>
          <Container>
              {app.todoLists.map((list) => {
                  return <TodoListComponent key={list.id} listId={list.id} listName={list.name}/>
              })}
          </Container>
      </div>
  );
};
export default Index;
