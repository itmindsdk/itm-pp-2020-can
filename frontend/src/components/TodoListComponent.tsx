﻿import React, {FunctionComponent, useCallback, useContext, useMemo, useState} from 'react';
import {
    CreateTodoListItemEntityCommand,
    TodoListItemEntityDto
} from "../../NSwagTS/backend-api";
import {Button, Col, ListGroup, Row, ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import TodoListItemComponent from "./TodoListItemComponent";
import {appContext} from "../contexts/AppContext";
import {context} from "../contexts/ClientContext";
import styles from "./TodoListComponent.module.css";
import buttonStyles from "./Button.module.css"

type Props = {
    listId : number
    listName : string
}

const TodoListComponent : FunctionComponent<Props> = ({ listId, listName }: Props) => {

    const app = useContext(appContext);
    const client = useContext(context);
    
    enum showStateEnum {
        ALL,
        ACTIVE,
        DONE
    }
    
    const doFilter = (item) => {
        if(itemFilter == showStateEnum.DONE) {
            return item.done;
        }
        if(itemFilter == showStateEnum.ACTIVE) {
            return !item.done;
        }
        return true;
    };
    
    const [itemFilter, setItemFilter] = useState(showStateEnum.ALL);
    
    const itemsToShow = useMemo(() => 
        app.todoListItems.filter((item) =>
            item.parentId == listId
        ), [app, itemFilter]);

    const itemsUndone = useMemo(() => 
            itemsToShow.reduce((acc, curr) =>
                acc + (curr.done ? 0 : 1), 0
            ), [itemsToShow]);

    const handleClick = useCallback(() => {
        client.todoListItemClient.create(new CreateTodoListItemEntityCommand({parentId: listId, description: "New Item", done: false}))
            .then((value) => {
                app.addTodoListItem(new TodoListItemEntityDto({id: value, parentId: listId, description: "New Item", done: false}));
            })
    },[client,app]);

    return(
        <ListGroup className={styles.list}>
            <ListGroup.Item active className={styles.listItem}>
                <Row className="align-items-center styles.listHeader">
                    <Col>
                        {listName}
                    </Col>
                    <Col>
                        <Button variant="primary" onClick={handleClick} className={`${buttonStyles.newList} ${buttonStyles.button}`}>Item+</Button>
                    </Col>
                </Row>
            </ListGroup.Item>
            {itemsToShow.map((item) => {
                if(doFilter(item)) {
                    return <ListGroup.Item className={styles.listItem}><TodoListItemComponent todoListItem={item}/></ListGroup.Item>
                }
            })}
            <ListGroup.Item className={styles.listItem}>
                <Row className="align-items-center">
                    <Col>
                        {itemsUndone} / {itemsToShow.length} items remaining
                    </Col>
                    <Col>
                        <ToggleButtonGroup type="radio" name="options" defaultValue={itemFilter} onChange={(e) => setItemFilter(e)} style={{float: "right"}}>
                            <ToggleButton className={buttonStyles.button} value={showStateEnum.ALL}>All</ToggleButton>
                            <ToggleButton className={buttonStyles.button} value={showStateEnum.ACTIVE}>Active</ToggleButton>
                            <ToggleButton className={buttonStyles.button} value={showStateEnum.DONE}>Done</ToggleButton>
                        </ToggleButtonGroup>
                    </Col>
                </Row>
            </ListGroup.Item>
        </ListGroup>
    );
};

export default TodoListComponent