﻿import React, {FunctionComponent, useContext, useState} from "react";
import {
    TodoListItemEntityDto, UpdateTodoListItemEntityCommand
} from "../../NSwagTS/backend-api";
import {Button, Col, InputGroup} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {appContext} from "../contexts/AppContext";
import {context} from "../contexts/ClientContext";
import buttonStyles from './Button.module.css';
import styles from './TodoListItemComponent.module.css'


type Props = {
    todoListItem : TodoListItemEntityDto
}

const TodoListItemComponent : FunctionComponent<Props> = ({ todoListItem }: Props) => {

    const [description, setDescription] = useState(todoListItem.description);
    const [done, setDone] = useState(todoListItem.done);

    const app = useContext(appContext);
    const client = useContext(context);

    const handleCheckChange = (checked) => {
        client.todoListItemClient.update(todoListItem.id, new UpdateTodoListItemEntityCommand({id: todoListItem.id, description: description, done: checked}))
            .then(() => {
                todoListItem.done = checked;
                app.updateTodoListItem(todoListItem)
            });
        setDone(checked);
    };

    const handleDescChange = (desc) => {
        client.todoListItemClient.update(todoListItem.id, new UpdateTodoListItemEntityCommand({id: todoListItem.id, description: desc, done: done}))
            .then(() => {
                todoListItem.description = desc;
                app.updateTodoListItem(todoListItem);
            });
        setDescription(desc);
    };
    
    const handleDelete = () => {
        client.todoListItemClient.delete(todoListItem.id)
            .then(() => {
                app.removeTodoListItem(todoListItem.id);
            });
    };

    return(
        <Form>
            <Form.Row className="align-items-center">
                <Col md="auto">
                    <Form.Check className={styles.checkMark} checked={todoListItem.done} aria-label="Check item off" onChange={ (e) => {
                        handleCheckChange(e.target.checked)
                    }}/>
                </Col>
                <Col>
                    <Form.Control className={styles.input} type="text" placeholder="" value={todoListItem.description} onChange={(e) => {
                        handleDescChange(e.target.value);
                    }}/>
                </Col>
                <Col md="auto">
                    <Button variant="outline-danger" onClick={handleDelete}>
                        Remove
                    </Button>
                </Col>
            </Form.Row>
        </Form>
    );
};

export default TodoListItemComponent
