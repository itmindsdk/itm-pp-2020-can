﻿import React, {FunctionComponent, useCallback, useContext, useState} from 'react';
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form"
import {CreateTodoListEntityCommand, TodoListEntityDto} from "../../NSwagTS/backend-api";
import {context} from "../contexts/ClientContext";
import {appContext} from "../contexts/AppContext";

const NavBarComponent : FunctionComponent = () => {
    const [name, setName] = useState("");
    
    const app = useContext(appContext);
    const client = useContext(context);
    
    const handleAddClick = useCallback(() => {
        if(name == "")
            return;
        client.todoListClient.create(new CreateTodoListEntityCommand({name}))
            .then((value) => {
                app.addTodoList(new TodoListEntityDto({id: value, name, items: []}));
            })
    },[client,app, name ]);
    
    return (
        <div style={{width: "100%", backgroundColor: "#f8f9fa", borderBottom: "3px solid rgb(238, 241, 243)", marginBottom: "1em"}}>
            <Container>
                <Navbar bg="light" expand="lg" style={{paddingRight: "0", paddingLeft: "0"}}>
                    <Navbar.Brand>Add todo list</Navbar.Brand>
                    <Form.Control type="text" placeholder="List name" value={name} onChange={(e) => setName(e.target.value)}/>
                    <Button variant="primary" onClick={handleAddClick} style={{marginLeft: "1em"}}>Add</Button>
                </Navbar>
            </Container>
        </div>
    );
};

export default NavBarComponent